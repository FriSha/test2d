﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

    public GameObject playerObject;
    public GameObject cameraObject;

    //public Cinemachine.CinemachineFramingTransposer cinemachine;

    public float moveCameraForward;
    public float cameraFall;

    public float runCameraDrag = 0;

    void Start()
    {

    }
	
	void Update ()
    {
        MoveCamera();

        //MoveCameraNew();
    }

    void MoveCamera()
    {
        float h = Input.GetAxis("Horizontal") * moveCameraForward;

        if (playerObject.transform.position.y>cameraObject.transform.position.y)
        {
            cameraObject.transform.position = new Vector3(playerObject.transform.position.x + (h + runCameraDrag) * moveCameraForward, playerObject.transform.position.y, playerObject.transform.position.z - 10);

        }
        else if (playerObject.transform.position.y<cameraObject.transform.position.y - cameraFall)
        {
            cameraObject.transform.position = new Vector3(playerObject.transform.position.x + (h + runCameraDrag) * moveCameraForward, playerObject.transform.position.y + cameraFall, playerObject.transform.position.z - 10);
        }
        else
        {
            cameraObject.transform.position = new Vector3(playerObject.transform.position.x + (h + runCameraDrag) * moveCameraForward, cameraObject.transform.position.y, playerObject.transform.position.z - 10);
        }

        /*if (grounded)
        {
            while (playerObject.transform.position.y < cameraObject.transform.position.y)
            {
                cameraObject.transform.position = new Vector3(playerObject.transform.position.x + h * moveCameraForward, cameraObject.transform.position.y - 0.0001f, playerObject.transform.position.z - 10);
            }
        }*/
    }

    void MoveCameraNew()
    {
        //cinemachine.m_
    }
}
