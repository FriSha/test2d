﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : PhysicsObject {

    private Animator anim;
    private SpriteRenderer spriteRenderer;

    public Text accelerateDebug;
    public Text currentLifeMeter;
    public Controller mainController;

    public float jumpSpeed = 7;
    public float maxSpeed = 7;
    public float maxRunSpeed = 12;

    public float hurtTime;
    public float hurtSpeed;
    public float invincibilityTime;

    public float maxLife;

    private bool isHurt = false;
    private float hurtTimer;
    private float hurtSpeedNow;
    private bool isInvincible = false;
    private float invincibilityTimer;

    private float currentLife;

    private bool doubleJump = false;
    private float accelerate = 0;

    void Start ()
    {
        anim = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        currentLife = maxLife;
    }

    protected override void ComputeVelocity()
    {
        if (!isHurt)
        {
            Move();

            Jump();

            Invincible();
        }
        else
        {
            HurtBack();
        }

        Animation();

        LifeMeter();

        ResetPlayer();
    }

    void Move()
    {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        float difference = maxRunSpeed - maxSpeed;

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetAxisRaw("Horizontal") != 0)
        {
            accelerate = Mathf.Round(accelerate * 10f) / 10f;
            accelerate = Mathf.Abs(accelerate) < difference ? accelerate + difference / 10 : accelerate;
        }
        else
        {
            accelerate = Mathf.Round(accelerate * 10f) / 10f;
            accelerate = Mathf.Abs(accelerate) > 0 ? accelerate - difference / 10 : accelerate;
        }

        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            move.x *= 0.7f;

            int flip = spriteRenderer.flipX ? -1 : 1;
            targetVelocity = move * maxSpeed + new Vector2((maxSpeed * 0.3f + accelerate) * flip, 0);
            mainController.runCameraDrag = accelerate * flip / difference;
        }

        accelerateDebug.text = "Accelerate:" + accelerate.ToString();

        Flip(move.x);
    }

    void Jump()
    {
        if (grounded)
        {
            doubleJump = true;
        }

        if (Input.GetButtonDown("Jump") && doubleJump)
        {
            velocity.y = jumpSpeed;
            if (!grounded) doubleJump = false;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }
    }

    void Flip(float moveX)
    {
        bool flipSprite = spriteRenderer.flipX ? moveX > 0 : moveX < 0;
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
    }

    void Animation()
    {
        anim.SetBool("Grounded", grounded);
        anim.SetFloat("Speed", Mathf.Abs(velocity.x));
        anim.SetFloat("vSpeed", velocity.y);
    }

    void ResetPlayer()
    {
        if (gameObject.transform.position.y < -10)
        {
            gameObject.transform.position = new Vector2(0, 0);
        }
    }

    void HurtBack()
    {
        int flip = spriteRenderer.flipX ? 1 : -1;
        targetVelocity = new Vector2(hurtSpeedNow * flip, 0);
        hurtTimer -= Time.deltaTime;
        hurtSpeedNow *= 0.8f;

        if (hurtTimer <= 0)
        {
            isHurt = false;
        }
    }

    void Invincible()
    {
        invincibilityTimer -= Time.deltaTime;
        if (invincibilityTimer <= 0)
        {
            isInvincible = false;
        }
    }

    void LifeMeter()
    {
        if (currentLife <= 0)
        {
            gameObject.transform.position = new Vector2(0, 0);
            currentLife = maxLife;
        }

        currentLifeMeter.text = "Life: " + currentLife;
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") && !isInvincible)
        {
            isHurt = true;
            hurtTimer = hurtTime;
            hurtSpeedNow = hurtSpeed;

            isInvincible = true;
            invincibilityTimer = invincibilityTime;

            currentLife -= 1;

            accelerate = 0;
        }
    }
}
