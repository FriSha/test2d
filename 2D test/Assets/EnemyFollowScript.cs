﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollowScript : MonoBehaviour {

    private EnemyTestScript enemy;

    // Use this for initialization
    void Start () {
        enemy = GetComponentInParent<EnemyTestScript>();
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            enemy.isMoving = false;
            enemy.isFollowing = true;

            enemy.sign = Mathf.Sign(gameObject.transform.position.x - collision.transform.position.x);
        }
        else
        {
            enemy.isFollowing = false;
        }
    }
}
