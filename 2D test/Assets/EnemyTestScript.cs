﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTestScript : PhysicsObject {

    public float waitTime;
    public float maxMove;
    public float moveSpeed;
    public float followSpeed;

    private float currentMove;
    private float moveDirection;

    public bool isFollowing = false;
    public float sign;

    public bool isMoving = false;

	void Start ()
    {
        StartCoroutine(RandomMove());
    }

    protected override void ComputeVelocity()
    {
        if (isMoving)
        {
            Move();
        }
        if (isFollowing)
        {
            Follow();
        }
    }

    IEnumerator RandomMove()
    {
        while (true)
        {
            isMoving = true;
            currentMove = Random.Range(0, maxMove);
            moveDirection = Mathf.Sign(Random.Range(-1, 1));

            yield return new WaitForSeconds(maxMove);
        }
    }

    void Move()
    {
        currentMove -= Time.deltaTime;

        targetVelocity = new Vector2(moveDirection * moveSpeed, 0);

        if (currentMove <= 0)
        {
            isMoving = false;
        }
    }

    void Follow()
    {
        targetVelocity = new Vector2(sign * followSpeed, 0);
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isMoving = false;
        }
    }
}
